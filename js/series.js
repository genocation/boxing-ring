var series = {
  "intense": [
    "1-1-2",
    "2-5-2",
    "3b-2-2b-3",
    "freestyle inside",
    "4-6-3",
    "2-3-2",
    "1-2-3b-4b",
    "1-2-1-2",
    "left-3b-2-right-2-3",
    "2b-2"
  ],
  "soft": [
    "1-1",
    "cover-1-cover-2",
    "cover-3-cover-4",
    "movement",
    "duck right-duck left",
    "work outside",
    "1-2-1-2",
    "2-cover-1-1-cover",
    "slipping",
    "br-3-bl-2",
    "1-1-1"
  ],
  "guides": {
    "1":"jab",
    "2":"cross",
    "3":"(dom) hook",
    "4":"(sub) hook",
    "5":"(dom) uppercut",
    "6":"(sub) uppercut",
    "1b":"jab to the body",
    "2b":"cross to the body",
    "3b":"(dom) hook to the body",
    "4b":"(sub) hook to the body",
    "5b":"(dom) uppercut to the body",
    "6b":"(sub) uppercut to the body",
    "right":"step right",
    "left":"step left",
    "br": "right cover",
    "bl": "left cover"
  }
}
