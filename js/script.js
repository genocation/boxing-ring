const startSeconds = 10
const intenseSeconds = 60
const softSeconds = 30
const restSeconds = 10
const seriesReps = 3

let counter
let secs = 0
let rep = 0
let intense
let soft
let paused = false


function pauseTraining() {
  paused = true
  $("#pause").html("Resume")
  $("#pause").removeClass("pause").addClass("resume")
}

function resumeTraining() {
  paused = false
  $("#pause").html("Pause")
  $("#pause").removeClass("resume").addClass("pause")
}

function secondsToClock(seconds) {
  let min = Math.floor(seconds / 60);
  let sec = seconds - min * 60;
  return [
    ("0" + min).slice(-2),
    ("0" + sec).slice(-2)
  ]
}

function printClock(seconds) {
  clock = secondsToClock(seconds)
  $("#clock").html(clock[0]+":"+clock[1])
}

function startCountdown(seconds, callback) {
  secs = seconds
  printClock(secs)
  if (counter) {
    clearInterval(counter)
  }
  counter = setInterval(function() {
    if (!paused) {
      secs -= 1
      printClock(secs)
      if (secs == 0) {
        clearInterval(counter)
        callback()
      }
    }
  }, 1000)
}

function getSeries(array) {
  let s = array[Math.floor(Math.random() * array.length)]
  let sarr = s.split('-')
  const garr = []
  sarr.forEach(e => {
    if (series.guides[e]) {
      garr.push(e + ": " + series.guides[e])
    } else {
      garr.push(e)
    }
  })
  return [s, garr]
}

function startRest() {
  rep += 1
  if (rep > seriesReps) {
    $("#training").removeClass("soft").addClass("rest")
    $("#script").html("Rest...")
    $("#guide").html("")
    startCountdown(restSeconds, function() { startRepetitions() })
  } else {
    startIntenseSeries()
  }
}

function startSoftSeries() {
  $("#training").removeClass("intense").addClass("soft")
  $("#script").html(soft[0])
  $("#guide").html(soft[1].join("</br>"))
  startCountdown(softSeconds, function() { startRest() })
}

function startIntenseSeries() {
  $("#training").removeClass("countdown rest soft").addClass("intense")
  $("#repetition").html("repetition: " + rep)
  $("#script").html(intense[0])
  $("#guide").html(intense[1].join("</br>"))
  startCountdown(intenseSeconds, function() { startSoftSeries() })
}

function startRepetitions() {
  rep = 1
  intense = getSeries(series.intense)
  soft = getSeries(series.soft)
  startIntenseSeries()
}

function startTraining() {
  $("#initial").hide()
  $("#training").removeClass("intense soft pause").addClass("countdown")
  $("#repetition").html("")
  $("#script").html("Get ready!")
  $("#guide").html("")
  $("#training").show()
  startCountdown(startSeconds, function() { startRepetitions() })
}

function stopTraining() {
  $("#training").hide()
  $("#initial").show()
  clearInterval(counter)
  secs = 0
  rep = 0
  intense = null
  soft = null
  paused = false
}

$(document).ready(function() {
  $("#start").click(function() {
    startTraining()
  })
  $("#stop").click(function() {
    stopTraining()
  })
  $("#pause").click(function() {
    if (paused) {
      resumeTraining()
    } else {
      pauseTraining()
    }
  })
})
